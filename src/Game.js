import BootState from './states/Boot';
import PreloadState from './states/Preload';
import MenuState from './states/Menu'
import PlayState from './states/Play';

class Game extends Phaser.Game {
  constructor() {
    super(960, 540, Phaser.AUTO, 'game');
    this.state.add('boot', BootState);
    this.state.add('preload', PreloadState);
    this.state.add('menu', MenuState)
    this.state.add('play', PlayState);

    this.state.start('boot');
  }
}

new Game();
