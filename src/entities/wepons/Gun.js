import Bullet from '../Bullet';

export default class Gun extends Phaser.Group {
    constructor({game}) {
        super(game, game.world, 'gun', false, true, Phaser.Physics.ARCADE);
        this.game = game;
        this.name = 'gun';
        //this.game.physics.arcade.enable(this)
        this.nextFire = 0;
        this.bulletSpeed = 600;
        this.fireRate = 500;
        this.damage = 34

        for (var i = 0; i < 64; i++)
        {
            this.add(new Bullet({
                game: this.game,
                key: 'bullet'
            }), true);
        }

        //return this;
    }
        //Phaser.Group.call(this, game, game.world, 'Single Bullet', false, true, Phaser.Physics.ARCADE);

    //Weapon.SingleBullet.prototype = Object.create(Phaser.Group.prototype);
    //Weapon.SingleBullet.prototype.constructor = Weapon.SingleBullet;

    fire(source) {
        if (this.game.time.time < this.nextFire) { return; }

        var x = source.x + 10;
        var y = source.y + 10;
        var angle = source.angle + 90;

        this.getFirstExists(false).fire(x, y, angle, this.bulletSpeed, 0, 0);

        this.nextFire = this.game.time.time + this.fireRate;

    };

};