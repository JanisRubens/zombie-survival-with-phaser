export default class Bullet extends Phaser.Sprite { 
    constructor({game, key}) {
        super(game, 0, 0, key);
        this.game = game;
        this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;
        //this.game.physics.arcade.enable(this);
        //this.game.add.existing(this);

        this.anchor.set(0.5);

        this.checkWorldBounds = true;
        this.outOfBoundsKill = true;
        this.exists = false;

        this.tracking = false;
        this.scaleSpeed = 0;
    }

       // Phaser.Sprite.call(this, game, 0, 0, key);
       
    fire(x, y, angle, speed, gx, gy) {

        gx = gx || 0;
        gy = gy || 0;

        this.reset(x, y);
        this.scale.set(1);
        this.game.physics.arcade.velocityFromAngle(angle, speed, this.body.velocity);

        this.angle = angle;

        this.body.gravity.set(gx, gy);

    };

    update() {

        if (this.tracking)
        {
            this.rotation = Math.atan2(this.body.velocity.y, this.body.velocity.x);
        }

        if (this.scaleSpeed > 0)
        {
            this.scale.x += this.scaleSpeed;
            this.scale.y += this.scaleSpeed;
        }
        //this.render()

    };

    render() {
        //this.game.debug.body(this);
        //this.game.debug.bodyInfo(this, 32, 32);  
    }



    };