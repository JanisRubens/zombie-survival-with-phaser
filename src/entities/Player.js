
import Gun from './wepons/Gun'

export default class Player extends Phaser.Sprite {
  constructor({game,x,y, key}) {
    super(game,x,y, key);
      this.game = game
      //sprite
      this.anchor.setTo(0.5, 0.5);
      this.animations.add('run', [0, 1, 2], true);
      this.angle = 1.57079633
      // Physics
      this.game.physics.arcade.enable(this);
      this.game.add.existing(this);
      //physical body
      this.body.enable = true;
      this.body.collideWorldBounds = true;
      this.body.setCircle(20, 4, 4);
      //characteristics
      this.health = 100;
      this.wepons = [];
      //this.wepons.push(new Wepon(this.game))
      this.wepon = new Gun({game:this.game});
  }

  //SETTER @HEALTH caping health @100
  set health( h ) {
    // ( h >= 100 ? h <= 0 ? 0 : h : 100)
    return this.body.health = (this.body.health || 0) + h;
  }

  get health() {
    return this.body.health
  }

  die() {
    //call animateOnce for die
    return this.destroy()
  }

  update() {
    
    const controls = this.getControls()
    this.body.velocity.x = this.body.velocity.y = 0;
    this.rotation = Player.fixRotation( this.game.physics.arcade.angleToPointer(this) );

    if (controls.LEFT) this.body.velocity.x -= 155;
    if (controls.RIGHT) this.body.velocity.x += 155;
    if (controls.UP) this.body.velocity.y -= 155;
    if (controls.DOWN) this.body.velocity.y += 155;
    if (controls.FIRE)
    {
        this.wepon.fire(this);
    }

    this.render()
        if ( this.health <= 0 ) this.die() //always last in loop
  }

  render() {
    //this.game.debug.text("HEALTH: "+this.health || '--', 20, 580, "#ffffff");  
    if (!!this.body.velocity.x || !!this.body.velocity.y) this.animations.play('run', 12, true);
    else this.animations.stop();
  }

  getControls() {
    const LEFT = this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)
      || this.game.input.keyboard.isDown(Phaser.Keyboard.A);

    const RIGHT = this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)
      || this.game.input.keyboard.isDown(Phaser.Keyboard.D);

    const UP = this.game.input.keyboard.isDown(Phaser.Keyboard.UP)
      || this.game.input.keyboard.isDown(Phaser.Keyboard.W)

    const DOWN = this.game.input.keyboard.isDown(Phaser.Keyboard.DOWN)
      || this.game.input.keyboard.isDown(Phaser.Keyboard.S)

    const JUMP = this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR);

    const FIRE = this.game.input.activePointer.isDown

      return {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        JUMP,
        FIRE
      }
    }
    
    //helpers
    //Forces the player sprite to be rotate + 90 deg
    static fixRotation( rotation ) {
      return rotation - 1.57079633
    }
}