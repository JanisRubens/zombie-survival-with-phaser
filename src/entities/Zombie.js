export default class Zombie extends Phaser.Sprite {
    constructor({game,x,y, key}) {
        super(game,x,y, key);
        this.game = game
        this.key = key
        //sprite
        this.anchor.setTo(0.5, 0.5);
        this.animations.add('run', [0, 1, 2], true);
        this.angle = 1.57079633



        // Physics
        this.game.physics.arcade.enable(this);
        this.game.add.existing(this);

            
        //collision box
        this.body.enable = true;
        this.body.collideWorldBounds = true;
        this.body.setCircle(20, 4, 4);     

    this.SPEED = 50; // missile speed pixels/second
    this.TURN_RATE = 10; // turn rate in degrees/frame
    this.attackRate = 1000;

  }

  set closestEnemy( enemy ) {
    this.enemy = enemy
  }

  get closestEnemy() {
    return this.closestEnemy;
  }
  
  findClosestEnemy( players ) {
    let closest = 999999
    let dist = 0
    players.forEach( (player) => {
        dist =  this.game.math.distance(this.x, this.y, player.x , player.y)
      if (closest > dist) {
          closest = dist;
          this.closestEnemy = player;
      }
    });
  }

  attack( enemy ) {
    if (this.game.time.time < (this.nextAttack || 0)) { return; }
        this.nextAttack = this.game.time.time + this.attackRate;
    return enemy.health = -10
  }

update() {
    
    //Game -> Math -> distance(x1, y1, x2, y2) → {number}
    let distance;
    if(this.enemy) {
         distance = this.game.math.distance(this.x, this.y, this.enemy.x, this.enemy.y)
  


    // Calculate the angle from the missile to the mouse cursor game.input.x
    // and game.input.y are the mouse position; substitute with whatever
    // target coordinates you need.
    var targetAngle = this.game.math.angleBetween(
        this.x, this.y,
        this.enemy.x, this.enemy.y
    );

    // Gradually (this.TURN_RATE) aim the missile towards the target angle
    if (this.rotation !== targetAngle) {
        // Calculate difference between the current angle and targetAngle
        var delta = targetAngle - this.rotation;

        // Keep it in range from -180 to 180 to make the most efficient turns.
        if (delta > Math.PI) delta -= Math.PI * 2;
        if (delta < -Math.PI) delta += Math.PI * 2;

        if (delta > 0) {
            // Turn clockwise
            this.angle += this.TURN_RATE;
        } else {
            // Turn counter-clockwise
            this.angle -= this.TURN_RATE;
        }

        // Just set angle to target angle if they are close
        if (Math.abs(delta) < this.game.math.degToRad(this.TURN_RATE)) {
            this.rotation = targetAngle;
        }
    }
      }

    //if we are close enough stop walking
 this.body.velocity.x = this.body.velocity.y = 0;
    if (distance > 25) {
      // Calculate velocity vector based on this.rotation and this.SPEED
      //(distance < 100 ? 2 : 1) if he gets close he dashes
      this.body.velocity.x = Math.cos(this.rotation) * this.SPEED * (distance < 100 ? 2 : 1);
      this.body.velocity.y = Math.sin(this.rotation) * this.SPEED * (distance < 100 ? 2 : 1);
    }

    if (distance < 5) this.attack(this.enemy)

    this.render()


};

render() {
    //this.game.debug.body(this);
    this.animations.play('run', 12, true)
}




}