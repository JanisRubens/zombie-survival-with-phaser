import Zombie from '../Zombie';

export default class NormalZombie extends Zombie {

    constructor({game,x,y, key}) {
        super({game,x,y, key});
        this.TURN_RATE = 100
        this.SPEED = 100;
        
    }

}