import NormalZombie from './zombieTypes/NormalZombie';

//new Group(game, parent, name, addToStage, enableBody, physicsBodyType)
export default class MobFactory extends Phaser.Group {
    constructor({game}) {
        super(game, game.world, 'mobFactory', false, true, Phaser.Physics.ARCADE);
        this.game = game;
        this.name = 'mobFactory';
    }

    spawn(level) {
        for (var i = 0; i < 5; i++) {
            this.add(new NormalZombie({
                game: this.game,
                x: (100 * i ),
                y: (100 * i ),
                key: 'zombie'
            }));
        }
    }
}