class Preload extends Phaser.State {
  create() {
    this.game.load.onLoadStart.add(this.loadStart, this);
    this.game.load.onFileComplete.add(this.fileComplete, this);
    this.game.load.onLoadComplete.add(this.loadComplete, this);

    this.loadingText = this.game.add.text(32, 32, 'Loading...', {fill: '#fff'});

    // Load your assets here
    //path needs to be as from build folder
    this.load.tilemap('tilemap', 'assets/tilemaps/map.json', null, Phaser.Tilemap.TILED_JSON);
    this.load.image('tilesheet', 'assets/images/tilesheet.png');
    this.game.load.spritesheet('s', 'assets/images/assets.png', 48, 48, 3);
    this.game.load.image('background','assets/images/debug-grid-1920x1920.png');
    this.game.load.image('sbullet', 'assets/images/shmup-bullet.png');
    this.game.load.image('bullet', 'assets/images/bullet.png');
    this.game.load.image('zombie', 'assets/images/zombie.png');

    this.game.load.start();
  }

  loadStart() {
    this.loadingText.setText('Loading...');
  }

  fileComplete(progress) {
    this.loadingText.setText('Loading: ' + progress + '%');
  }

  loadComplete() {
    this.loadingText.setText('Load Complete');
    this.game.state.start('play');
  }
}

export default Preload;
