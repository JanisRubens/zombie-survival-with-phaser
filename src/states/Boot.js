class Boot extends Phaser.State {
  create() {
    this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    this.game.scale.parentIsWindow = true;
    this.game.stage.backgroundColor = '#000';
    this.game.state.start('preload');
  }
}

export default Boot;
