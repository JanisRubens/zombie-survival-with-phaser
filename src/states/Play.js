import Player from '../entities/Player'
import Zombie from '../entities/Zombie'
import NormalZombie from '../entities/zombieTypes/NormalZombie';
import MobFactory from '../entities/MobFactory'

class Play extends Phaser.State {
    preload() {
        //needed for Fps meter
        this.game.time.advancedTiming = true;
    }

    create() {
        // Capture controls to prevent unwanted scrolling
        this.game.input.keyboard.addKeyCapture([
            Phaser.Keyboard.LEFT,
            Phaser.Keyboard.RIGHT,
            Phaser.Keyboard.UP,
            Phaser.Keyboard.DOWN,
            Phaser.Keyboard.SPACEBAR,
        ]);

        this.map = this.game.add.tilemap('tilemap');
        //the first parameter is the tileset name as specified in Tiled, the second is the key to the asset
        this.map.addTilesetImage('tileset', 'tilesheet'); //tileset-> ref in map.json // tilesheet => key in preload

        //create layer
        this.backgroundLayer = this.map.createLayer('background');
        this.blockedLayer = this.map.createLayer('collision');

        //collision on blockedLayer
        this.map.setCollisionBetween(1, 500, true, 'collision');
        //resizes the game world to match the layer dimensions
        this.backgroundLayer.resizeWorld();

        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.level = 0;
        this.mobFactory = new MobFactory({game: this.game});
        this.player = new Player({
            game: this.game,
            x: 100,
            y: 100,
            key: 's'
        })
    
        this.game.camera.follow(this.player, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
    }

    //BE FUCKING SMART WHEN YOU PLACE SHIT IN HERE, ONE MISTAKE CAN MAKE THE GAME STUPID BUGS
    update() {   
        this.game.physics.arcade.collide(this.mobFactory);
	  
        if (!this.mobFactory.total) {
            this.level++
            this.mobFactory.spawn( this.level )
        }
        if (this.mobFactory.children) {
    	    this.mobFactory.children.forEach( z => {
			    z.findClosestEnemy([this.player])
			    this.game.physics.arcade.collide(this.player, z, z.attack.bind(z, this.player))
			    this.game.physics.arcade.collide(z, this.player.wepon, this.collisionHandler, this.processHandler, this)
			    this.game.physics.arcade.collide(z, this.blockedLayer)
     	    })
        }

        this.game.physics.arcade.collide(this.player, this.blockedLayer);
    }

    render() {
        //fps
        //this.game.debug.text(this.game.time.fps || '--', 2, 14, "#00ff00");  
        //this.game.debug.bodyInfo(this.player, 32, 32);
        //this.game.debug.body(this.player);
        //this.game.debug.bodyInfo(this.z, 32, 160);
    }

    processHandler (player, veg) {
        console.log('dieee biaatch')
    }

    collisionHandler (z, bullet) {
	    bullet.kill();
        z.kill();
    }
}

export default Play;
