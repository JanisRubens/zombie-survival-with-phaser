# Comands:

`npm run start` - for development
`npm run production` - for production build

# Powered by phaser-es6-boilerplate:

  "repository": {
    "type": "git",
    "url": "https://github.com/belohlavek/phaser-es6-boilerplate"
  },
  "author": "Daniel Belohlavek",